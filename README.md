# [design.wikimedia.org/blog](https://design.wikimedia.org/blog)

Imported from  https://gerrit.wikimedia.org/r/plugins/gitiles/design/blog/.


<!-- TOC GFM -->

- [design.wikimedia.org/blog](#designwikimediaorgblog)
  - [Prerequisites](#prerequisites)
  - [Running locally](#running-locally)
  - [Deploying to production](#deploying-to-production)
  - [Development](#development)
    - [Ignored assets](#ignored-assets)
    - [Link attributes](#link-attributes)
  - [Blubber container image](#blubber-container-image)
    - [Local development](#local-development)
    - [Publish new image version](#publish-new-image-version)
  - [Deploy changes](#deploy-changes)

<!-- /TOC -->

## Prerequisites

* [Ruby](https://www.ruby-lang.org/) (Ruby 2.6+)
* [Bundler](https://bundler.io/) (if missing, install with `gem install bundler`)

## Running locally 

* Install Jekyll and plugins:
  ```
  bundle install
  ```

* Update Jekyll and plugins versions:
  ```
  bundle update
  ```

* Start a Jekyll server for local development at at <http://localhost:4000/blog/> (automatically watches for changes and updates in real-time):
  ```
  bundle exec jekyll serve
  ```

## Deploying to production
Design blog server points to the '_site/' directory, so Jekyll needs to build
in order to show the latest output.

* To build the production version to commit to Git and deploy to live server:
  ```
  bundle exec jekyll build -d _site/
  ```

## Development

### Ignored assets
Jekyll ignores files and directories that are prefixed by `_` and everything else is copied to `_site`.

`_src` directories inside `assets/` contain source assets used to generate the assets that go on the site.

### Link attributes

The Markdown syntax `[Name](url){:target='_blank' rel='nofollow'}` can be used to add attributes to the link element.
Note that single quotes are required when Markdown is rendered inside a Jekyll include because the build will fail when using double quotes or raw HTML inside a variable.

## Blubber container image

Blubber images for design-blog page.

### Local development

Build image locally:
```
DOCKER_BUILDKIT=1 docker build --target production -f .pipeline/blubber.yaml .
```

Run image:
```
docker run -p 8080:8080 <image name>
```

Visit `http://127.0.0.1:8080/`

### Publish new image version

To create a new image version merge your change into the master branch.

This triggers the publish-image pipeline. Image is available at `docker-registry.wikimedia.org/repos/sre/miscweb/design-blog:<timestamp>`

## Deploy changes

- Merge changes to master branch.

- Update image version on [deployment-charts](https://gerrit.wikimedia.org/r/plugins/gitiles/operations/deployment-charts/%2B/refs/heads/master/helmfile.d/services/miscweb/values-design-blog.yaml#3) with [latest value](https://gitlab.wikimedia.org/repos/sre/miscweb/design-blog/-/jobs/). _The value is in the `#13` step in the job 'publish-image'._

- Add wmf-sre-collab (group) as reviewer and wait for merge.

- Deploy to production from the deployment server. 

See [WikiTech: Miscweb](https://wikitech.wikimedia.org/wiki/Miscweb#Deploy_to_Kubernets).
